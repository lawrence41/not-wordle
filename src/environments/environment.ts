// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'not-wordle-b0216',
    appId: '1:841274765547:web:2937609de35468135bceae',
    databaseURL: 'https://not-wordle-b0216-default-rtdb.firebaseio.com',
    storageBucket: 'not-wordle-b0216.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyASvLce6Yxa2nNu4SnOVb4HN5YyLRCW_l8',
    authDomain: 'not-wordle-b0216.firebaseapp.com',
    messagingSenderId: '841274765547',
    measurementId: 'G-40TXG90FBV',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
