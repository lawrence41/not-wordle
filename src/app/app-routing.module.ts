import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PlayComponent} from './play/play.component';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  {path: '', component: PlayComponent},
  {path: 'menu', component: MainMenuComponent},
  {path: 'login', component: LoginComponent },
  {path: '**', component: PlayComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
