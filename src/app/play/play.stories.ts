import {PlayComponent} from "./play.component";
import {Meta, Story} from "@storybook/angular";

export default {
  title: 'template/play',
  component: PlayComponent
} as Meta

export const Template: Story<PlayComponent> = (args: PlayComponent) => ({});
export const Default: Story<PlayComponent> = Template.bind({})
