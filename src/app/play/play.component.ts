import {
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';
import { PlayWordComponent } from '../components/word/play-word.component';
import { Router } from '@angular/router';
import { GameLogicService } from '../game-logic.service';
import {AppStore} from "../app.store";

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit {
  title = 'not-wordle';
  firstWord = 'Hello';
  @ViewChildren(PlayWordComponent)
  wordComponents!: QueryList<PlayWordComponent>;
  wordGuessFormArray = new FormArray([
    new FormControl(''),
    new FormControl(''),
    new FormControl(''),
    new FormControl(''),
    new FormControl(''),
    new FormControl(''),
  ]);
  public activeRowIndex = 0;

  constructor(
    public router: Router,
    public changeDetectorRef: ChangeDetectorRef,
    public gameLogicService: GameLogicService,
    public store: AppStore
  ) {}

  get wordGuessFormControls(): FormControl[] {
    return this.wordGuessFormArray.controls as FormControl[];
  }

  ngOnInit() {
    this.wordGuessFormArray.disable({ emitEvent: false });
    this.wordGuessFormArray.at(this.activeRowIndex)?.enable();
  }

  async submitGuess() {
    const currentGuess = this.wordGuessFormArray.at(this.activeRowIndex).value;
    const result = this.gameLogicService.guess(currentGuess.toLowerCase());
    if (result) {
      if (result.every((n) => n === 2)) {
        this.store.updateMainMenuTitle([[...'good'], [...'wordle']])
        this.markRowAsSubmitted(result);
        this.router.navigate(['menu']);
      } else if (this.activeRowIndex < this.wordGuessFormArray.length - 1) {
        this.advanceRow(result);
      } else {
        this.markRowAsSubmitted(result);
        this.store.updateMainMenuTitle(`the word was ${this.gameLogicService.get_answer()}`.split(' ').map(word => [...word]))
        this.router.navigate(['menu']);

      }
    } else {
      console.log('word not in dictionary');
    }
  }

  private markRowAsSubmitted(result: Uint32Array) {
    (this.wordComponents.get(this.activeRowIndex) as PlayWordComponent).tileStates =
      result;
    this.wordGuessFormArray.at(this.activeRowIndex)?.disable();
  }

  private advanceRow(result: Uint32Array) {
    this.markRowAsSubmitted(result);
    this.activeRowIndex += 1;
    this.wordGuessFormArray.at(this.activeRowIndex).enable();
  }
}
