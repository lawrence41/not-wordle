import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { GameLogicService } from '../game-logic.service';
import { AppStore } from '../app.store';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainMenuComponent implements OnInit {

  constructor(
    public gameLogicService: GameLogicService,
    public store$: AppStore
  ) {}

  ngOnInit(): void {
  }

  newGame() {
    this.gameLogicService.reset();

  }
}
