import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { AppViewModel } from './app.model';

@Injectable()
export class AppStore extends ComponentStore<AppViewModel> {
  updateMainMenuTitle = this.updater(
    (state, mainMenuTitle: string[][]) => ({ mainMenuTitle })
  );
  mainMenuTitle$ = this.select(({mainMenuTitle}) => mainMenuTitle);
  constructor() {
    super({
      mainMenuTitle: [ [..."not"], [..."wordle"] ]
    });
  }

  vm$ = this.select( (state) => ({...state}) );

}
