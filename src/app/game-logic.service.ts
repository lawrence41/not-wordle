import {Injectable} from "@angular/core";
import {Game} from "wasm-wordle";
import {wordList} from "./word-list";

@Injectable({providedIn: 'root'})
export class GameLogicService {
  private game!: Game;
  constructor() {
    this.game = new Game(wordList);
  }

  reset () {
    this.game.new_game();
  }
  get_answer() {
    return this.game.get_answer().toUpperCase();
  }
  guess(currentGuess: string): Uint32Array | undefined {
    return this.game.guess(currentGuess)
  }
}
