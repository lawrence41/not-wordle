import {
  componentWrapperDecorator,
  Meta,
  moduleMetadata,
  Story,
} from '@storybook/angular';
import { HeaderComponent } from './header.component';
import { Component } from '@angular/core';
import { DynamicWordComponent } from '../dynamic-word/dynamic-word.component';
import { LetterTileComponent } from '../letter-tile/letter-tile.component';

export default {
  title: 'molecule/header',
  component: HeaderComponent,
  decorators: [
    moduleMetadata({
      declarations: [
        HeaderComponent,
        DynamicWordComponent,
        LetterTileComponent,
      ],
    }),
  ],
} as Meta;

export const Template: Story = (args: any) => ({ props: { ...args } });

export const HeaderWithDynamicWord = () => ({
  template: `<app-header><dynamic-word style="display: flex;" word="not wordle"></dynamic-word></app-header>`,
});
