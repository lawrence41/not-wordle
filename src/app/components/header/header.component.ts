import {Component} from "@angular/core";

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  template: '<header><ng-content></ng-content></header>'
})
export class HeaderComponent {

  constructor() {
  }
}
