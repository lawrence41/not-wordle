import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  HostListener,
  Input,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { LetterTileComponent } from '../letter-tile/letter-tile.component';
import { Observable } from 'rxjs';
import {
  ControlValueAccessor,
  FormArray,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

export type FiveLetterWord = [string, string, string, string, string];
export type FiveLetterWordChange = [
  Observable<string>,
  Observable<string>,
  Observable<string>,
  Observable<string>,
  Observable<string>
];

export function generateWordControls(value: string = '', amount = 5) {
  const controls = [];
  for (let i = 0; i < amount; i++) {
    controls.push(new FormControl(value[i] ?? ''));
  }
  return controls;
}

export function getLetterByIndex(word: string, index: number) {
  return word[index] ?? '';
}

@Component({
  selector: 'app-word',
  templateUrl: './play-word.component.html',
  styleUrls: ['./play-word.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PlayWordComponent),
      multi: true,
    },
  ],
})
export class PlayWordComponent implements AfterViewInit, ControlValueAccessor {
  constructor(public changeDetectorRef: ChangeDetectorRef) {}

  wordFormArray: FormArray = new FormArray(generateWordControls());
  @Output()
  submit: EventEmitter<void> = new EventEmitter<void>();
  wordFormGroup = new FormGroup({
    word: this.wordFormArray,
  });
  @ViewChildren(LetterTileComponent)
  letterTiles!: QueryList<LetterTileComponent>;

  @Input()
  autoFocus: boolean = false;
  public submitted: boolean = false;

  @HostListener('click')
  onClick() {
    this.onTouched();
  }
  onChange: any = (): void => {};
  onTouched: any = (): void => {};
  tileStates: Uint32Array = new Uint32Array([0, 0, 0, 0, 0]);

  setDisabledState(isDisabled: boolean) {
    if (isDisabled) {
      this.wordFormArray.disable();
    } else {
      this.wordFormArray.enable();
      if (this.autoFocus) {
        this.letterTiles.get(0)?.focus();
      }
    }
  }

  writeValue(value: string): void {
    value
      .trim()
      .split('')
      .map((char, i) =>
        this.wordFormArray.setControl(i, new FormControl(char))
      );
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  get letterControls(): FormControl[] {
    return this.wordFormArray.controls as FormControl[];
  }

  @HostListener('keyup', ['$event'])
  public keyUp($event: KeyboardEvent): void {
    if (
      $event.key === 'Enter' &&
      this.wordFormArray.value.join('').length === 5
    ) {
      this.submitted = true;
      this.submit.next(this.wordFormArray.value.join(''));
    }
  }

  ngAfterViewInit(): void {
    this.wordFormArray.valueChanges.subscribe((value) => {
      this.onChange(this.wordFormArray.value.join(''));
    });
  }

  focusNextTile(index: number) {
    if (index < 4 && this.letterTiles.get(index + 1)) {
      this.letterTiles.get(index + 1)?.focus();
    }
  }

  focusPreviousTile(index: number) {
    if (index > 0 && this.letterTiles.get(index - 1)) {
      this.wordFormArray.at(index - 1)?.setValue('');
      this.letterTiles.get(index - 1)?.focus();
    }
  }
}
