import { PlayWordComponent } from './play-word.component';
import {moduleMetadata} from "@storybook/angular";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

export default {
  component: PlayWordComponent,
  title: 'play/molecule/word',
  args: {
    word: 'hello world',
  },
  decorators: [
    moduleMetadata({
      imports: [CommonModule, FormsModule, ReactiveFormsModule],
    }),
  ],
};

export const Template = ({tileStates}: PlayWordComponent) => ({ props: { tileStates } });
Template.args = { props: { word: 'hello' } };
export const Default = Template.bind({});
