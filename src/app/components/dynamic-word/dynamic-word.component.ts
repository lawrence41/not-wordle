import {Component, Input} from "@angular/core";

@Component({
  selector: 'dynamic-word',
  templateUrl: './dynamic-word.component.html',
  styleUrls: ['./dynamic-word.component.scss']
})
export class DynamicWordComponent {
  @Input() word!: string;
}