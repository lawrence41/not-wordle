import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { expect } from '@storybook/jest';
import { within } from '@storybook/testing-library';
import { DynamicWordComponent } from './dynamic-word.component';
import { LetterTileComponent } from '../letter-tile/letter-tile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

export default {
  title: 'molecule/DynamicWord',
  component: DynamicWordComponent,
  decorators: [
    moduleMetadata({
      declarations: [LetterTileComponent, DynamicWordComponent],
      imports: [ReactiveFormsModule, FormsModule, CommonModule],
    }),
  ],
} as Meta;

export const Template: Story<DynamicWordComponent> = (
  args: Partial<DynamicWordComponent>
) => ({ props: { ...args } });

export const Default = Template.bind({});
Default.args = {
  word: 'hello',
};
Default.play = ({ canvasElement }) => {
  const canvas = within(canvasElement);
  // Check to see that there are 5 letter-tile components inside the canvas
  let inputs = canvas.getAllByRole('textbox');
  expect(canvas.getAllByRole('textbox').length).toBe(
    Default.args?.word?.length
  );
};
