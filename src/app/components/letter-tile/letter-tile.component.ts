import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'app-letter-tile',
  templateUrl: './letter-tile.component.html',
  styleUrls: ['./letter-tile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LetterTileComponent),
      multi: true,
    },
  ],
})
export class LetterTileComponent
  implements OnInit, AfterViewInit, ControlValueAccessor
{
  @Input()
  letter!: string;
  public control!: FormControl;
  @ViewChild('hiddenInput')
  hiddenInput!: ElementRef;
  focused = false;
  @Input()
  public index: number = -1;
  @Output()
  forward$: EventEmitter<void> = new EventEmitter<void>();
  @Output()
  backward$: EventEmitter<void> = new EventEmitter<void>();
  @Input()
  submitted!: boolean;
  disabled!: boolean;
  constructor(
    @Inject(DOCUMENT) public document: Document,
    public cdr: ChangeDetectorRef
  ) {}
  @Input()
  tileState: number = 0;

  @HostBinding('class.submitted') get _submitted(): boolean {
    return this.submitted ?? false;
  }
  @HostBinding('class.focused') get classFocused() {
    return this.focused;
  }
  @HostBinding('class.disabled') get _disabled(): boolean | null {
    return this.disabled;
  }
  @HostBinding('class.in-word') get _inWord(): boolean | null {
    return this.tileState > 0 && this.disabled;
  }

  @HostBinding('class.correct-position')
  get _correctPosition(): boolean | null | undefined {
    return this.tileState === 2 && this.disabled;
  }

  @HostListener('click', ['$event'])
  click(): void {
    if (!this.disabled) {
      this.focus();
    }
  }

  @HostListener('input', ['$event'])
  input(e: InputEvent): void {
    const newLetter = e.data?.toUpperCase() ?? '';
    if (!this.disabled) {
      if (/^[A-z]$/i.test(newLetter)) {
        this.onChange(newLetter);
        this.letter = newLetter;
        this.forward$.next();
      }
    }
  }
  @HostListener('keyup', ['$event'])
  keyUp(e: KeyboardEvent) {
    if (e.key?.toUpperCase() === 'BACKSPACE') {
      this.letter = '';
      this.onChange('');
      this.backward$.next();
    }
  }

  focus() {
    const { nativeElement } = this.hiddenInput;
    nativeElement.focus();
    this.onTouched();
  }

  ngOnInit(): void {}

  ngAfterViewInit() {}

  setFocused() {
    this.focused =
      this.document.activeElement === this.hiddenInput.nativeElement;
  }

  /**
   * Used to register onChange function for ControlValueAccessor
   * @param fn
   */
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  /**
   * Used to register onTouched function for ControlValueAccessor
   * @param fn
   */
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * Used to set disabled state from formControl for ControlValueAccessor
   * @param isDisabled
   */
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  /**
   * Used to write a value from a formControl for ControlValueAccessor
   */
  writeValue(value: string): void {
    this.letter = value;
  }
  onChange: any = () => {};
  onTouched: any = () => {};
}
