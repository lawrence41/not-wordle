import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { LetterTileComponent } from './letter-tile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CommonModule} from "@angular/common";

export default {
  title: 'atom/letter',
  component: LetterTileComponent,
  decorators: [
    moduleMetadata({
      imports: [CommonModule, FormsModule, ReactiveFormsModule],
    }),
  ],
} as Meta;

export const Template: Story<LetterTileComponent> = ({
  letter,
  focused,
  submitted,
  disabled,
  tileState
}: LetterTileComponent) => ({
  props: {
    tileState,
    disabled,
    letter,
    focused,
    submitted,
  },
});

export const Default = Template.bind({});
Default.args = {letter: 'L'};
