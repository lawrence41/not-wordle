const fs = require("fs");

let buffer = new Buffer("");

fs.readFile("./src/assets/words_alpha.txt", null, (e, buffer) => {
  fs.writeFile("./src/assets/word-list.json", " ", () => {});
  const words = buffer.toString("utf-8").split("\r\n");
  const data = JSON.stringify(words.filter(word => word.length === 5));
  fs.writeFile("./src/assets/word-list.json", data, () => {});
});
