module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/addon-docs"
  ],
  "framework": "@storybook/angular",
  "core": {
    "builder": "@storybook/builder-webpack5"
  },
  webpackFinal: async (webpack) => {
    if (!webpack.hasOwnProperty('experiments')) {
      webpack.experiments = {};
    }
    webpack.experiments.futureDefaults = true;
    return webpack;
  }
}
